const baseUrl = 'http://127.0.0.1:3000';

function login() {
    // read ajax for vanilla js
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    const url = baseUrl + '/login';

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        username,
        password
    }));

    xhr.onload = () => {
        if (xhr.status === 200) {
            alert('You have logged in successfully')
        } else {
            alert('invalid username or password');
        }
    }
}